import React, { Component } from 'react'
import PropTypes from 'prop-types'
export default class PropTypeClassComponent extends Component {
  render () {
    return (
      <>
        <div className="classComponents">
          <div>
            <details>
              <summary>The Details of Developer</summary>
              <h3>Name: {this.props.class}</h3>
          <h3>Age:{this.props.age}</h3>
          <h3>Profession: {this.props.profession}</h3>
          <h3>Language: {this.props.language.join(', ')}</h3>
          <h3>Count: {this.props.count}</h3>
          <h3>Address {this.props.details.address}</h3>
          <h3>Phone{this.props.details.Phone}</h3>
            </details>
          </div>
        </div>
      </>
    )
  }
}

PropTypeClassComponent.propTypes = {
  class: PropTypes.string,
  age: PropTypes.node,
  count: PropTypes.number,
  profession: PropTypes.string,
  language: PropTypes.array,
  details: PropTypes.shape({
    address: PropTypes.string,
    Phone: PropTypes.number
  })
}
