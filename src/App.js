import React, { useState } from 'react'
import './App.css'
import Developer from './Developer'
import Student from './Student'

function App () {
  const [count, setCount] = useState(0)
  const handleIncrement = () => {
    setCount(count + 1)
  }
  return (
    <div className="App">
      <Developer
        name="Ashish"
        age={23}
        profession="WebDeveloper"
        language={['Html', 1, 'Css', 'javascript', 'ReactJs']}
        stillJob={false}
        count={count}
        onclick={handleIncrement}
        student={<Student />}
        details={{
          address: '114 New RanchhodRai',
          Phone: 9664956491,
          gender: 'male'
        }}
      />

      <button onClick={handleIncrement}>Increment</button>
    </div>
  )
}

export default App
