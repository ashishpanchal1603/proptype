import React from 'react'
import PropTypes from 'prop-types'

function Developer ({
  name,
  age,
  profession,
  language,
  stillJob,
  count,
  student,
  details
}) {
  return (
    <>
      <div>
        <details>
          <summary>The Details of Developer</summary>
          <h3>Name: {name}</h3>
          <h3>Age:{age}</h3>
          <h3>Profession: {profession}</h3>
          <h3>Language: {language.join(', ')}</h3>
          <h3>
            Getting Job or no :
            {stillJob ? 'now You are working Job' : 'Now you Find a Job'}
          </h3>
          <h3>Count: {count}</h3>
          <h3>Function: {onclick}</h3>
          {console.log('function', onclick)}
          <h3>Student Components {student}</h3>
          <h3>Address {details.address}</h3>
          <h3>Phone{details.Phone}</h3>
        </details>
      </div>
    </>
  )
}

Developer.propTypes = {
  name: PropTypes.string.isRequired,
  age: PropTypes.node,
  profession: PropTypes.string,
  language: PropTypes.array,
  // language: PropTypes.arrayOf(PropTypes.string,PropTypes.number),
  stillJob: PropTypes.bool,
  onclick: PropTypes.func,
  student: PropTypes.elementType,
  // details: PropTypes.object,
  count: PropTypes.number,

  // in shape like not address are match but work as it is example name is address but write a add so can write
  details: PropTypes.shape({
    address: PropTypes.string,
    Phone: PropTypes.number
  })
}

Developer.defaultProps = {
  name: 'abc',
  age: 18
}
export default Developer
